const exactTranslation = {

    // Yaku

    '流局滿貫': {
        'DEFAULT': 'Nagashi Mangan',
    },
    '立直': {
        'DEFAULT': 'Riichi',
        'EMA_ENG': 'Riichi',
        'ENG': 'Ready-riichi',
    },
    '赤ドラ': {
        'DEFAULT': 'Akadora',
        'EMA_ENG': 'Red fives',
    },
    '斷么九': {
        'DEFAULT': 'Tanyao',
        'EMA_ENG': 'All simples',
    },
    '断幺九': {
        'DEFAULT': 'Tanyao',
        'EMA_ENG': 'All simples',
    },
    '平和': {
        'DEFAULT': 'Pinfu',
        'EMA_ENG': 'Pinfu',
        'ENG': 'Pinfu no-point',
    },
    '門前清自摸和': {
        'DEFAULT': 'Menzen tsumo',
        'EMA_ENG': 'Self-drawn fully concealed',
    },
    '裏ドラ': {
        'DEFAULT': 'Uradora',
        'EMA_ENG': 'Uradora',
    },
    '一發': {
        'DEFAULT': 'Ippatsu',
        'EMA_ENG': 'One-shot',
    },
    '一発': {
        'DEFAULT': 'Ippatsu',
        'EMA_ENG': 'One-shot',
    },
    '場風 東': {
        'DEFAULT': 'Bakaze Ton',
        'EMA_ENG': 'E Round Wind',
    },
    '役牌 中': {
        'DEFAULT': 'Yakuhai Chun',
        'EMA_ENG': 'Red dragons',
    },
    '役牌 發': {
        'DEFAULT': 'Yakuhai Hatsu',
        'EMA_ENG': 'Green dragons',
    },
    '役牌 白': {
        'DEFAULT': 'Yakuhai Haku',
        'EMA_ENG': 'White dragons',
    },
    '混一色': {
        'DEFAULT': 'Honitsu',
        'EMA_ENG': 'Half flush',
    },
    '一盃口': {
        'DEFAULT': 'Iipeikou',
        'EMA_ENG': 'Pure Double Chow',
    },
    '三色同順': {
        'DEFAULT': 'Sanshoku doujun',
        'EMA_ENG': 'Mixed Triple Chow',
    },
    '對對和': {
        'DEFAULT': 'Toitoi',
        'EMA_ENG': 'All pungs',
    },
    '対々和': {
        'DEFAULT': 'Toitoi',
        'EMA_ENG': 'All pungs',
    },
    '自風 東': {
        'DEFAULT': 'Jikaze Ton',
        'EMA_ENG': 'E Seat wind',
    },
    '七對子': {
        'DEFAULT': 'Chiitoitsu',
        'EMA_ENG': '7 pairs',
    },
    '七対子': {
        'DEFAULT': 'Chiitoitsu',
        'EMA_ENG': '7 pairs',
    },
    '自風 南': {
        'DEFAULT': 'Jikaze Nan',
        'EMA_ENG': 'S Seat wind',
    },
    '自風 西': {
        'DEFAULT': 'Jikaze Sha',
        'EMA_ENG': 'W Seat wind',
    },
    '四槓子': {
        'DEFAULT': 'Suukantsu',
        'EMA_ENG': '4 kongs',
    },
    '自風 北': {
        'DEFAULT': 'Jikaze Pei',
        'EMA_ENG': 'N Seat wind',
    },
    '清一色': {
        'DEFAULT': 'Chiniisou',
        'EMA_ENG': 'Full flush',
    },
    '三暗刻': {
        'DEFAULT': 'Sanankou',
        'EMA_ENG': '3 concealed pungs',
    },
    '河底撈魚': {
        'DEFAULT': 'Houtei raoyui',
        'EMA_ENG': 'Bottom of the sea (discard)',
    },
    '混全帶么九': {
        'DEFAULT': 'Chanta',
        'EMA_ENG': 'Outside hand',
    },
    '混全帯幺九': {
        'DEFAULT': 'Chanta',
        'EMA_ENG': 'Outside hand',
    },
    '海底摸月': {
        'DEFAULT': 'Haitei raoyue',
        'EMA_ENG': 'Bottom of the sea (drawn)',
    },
    '嶺上開花': {
        'DEFAULT': 'Rinshan kaihou',
        'EMA_ENG': 'After a kong',
    },
    '兩立直': {
        'DEFAULT': 'Double riichi',
        'EMA_ENG': 'Double riichi',
    },
    '両立直': {
        'DEFAULT': 'Double riichi',
        'EMA_ENG': 'Double riichi',
    },
    '場風 南': {
        'DEFAULT': 'Bakaze Nan',
        'EMA_ENG': 'S Round wind',
    },
    '場風 西': {
        'DEFAULT': 'Bakaze Sha',
        'EMA_ENG': 'W Round wind',
    },
    '場風 北': {
        'DEFAULT': 'Bakaze Pei',
        'EMA_ENG': 'N Round wind',
    },
    '小三元': {
        'DEFAULT': 'Shousangen',
        'EMA_ENG': 'Little 3 dragons',
    },
    '混老頭': {
        'DEFAULT': 'Honroutou',
        'EMA_ENG': 'Only terminals & honours',
    },
    '槍槓': {
        'DEFAULT': 'Chankan',
        'EMA_ENG': 'Robbing a kong',
    },
    '一氣通貫': {
        'DEFAULT': 'Ikkitsuukan',
        'EMA_ENG': 'Pure straight',
    },
    '一気通貫': {
        'DEFAULT': 'Ikkitsuukan',
        'EMA_ENG': 'Pure straight',
    },
    '二盃口': {
        'DEFAULT': 'Ryanpeikou',
        'EMA_ENG': 'Twice pure double chow',
    },
    '三色同刻': {
        'DEFAULT': 'Sanshoku doukou',
        'EMA_ENG': 'Triple Pung',
    },
    '大三元': {
        'DEFAULT': 'Daisangen',
        'EMA_ENG': '3 dragons',
    },
    '純全帶么九': {
        'DEFAULT': 'Junchan',
        'EMA_ENG': 'Terminals in all sets',
    },
    '純全帯幺九': {
        'DEFAULT': 'Junchan',
        'EMA_ENG': 'Terminals in all sets',
    },
    '四暗刻': {
        'DEFAULT': 'Suuankou',
        'EMA_ENG': '4 concealed pungs',
    },
    '國士無雙': {
        'DEFAULT': 'Kokushi musou',
        'EMA_ENG': '13 orphans',
    },
    '國士無雙13面': {
        'DEFAULT': 'Kokushi musou 13 men',
        'EMA_ENG': '13 orphans (13 wait)',
    },
    '国士無双': {
        'DEFAULT': 'Kokushi musou',
        'EMA_ENG': '13 orphans',
    },
    '国士無双１３面': {
        'DEFAULT': 'Kokushi musou 13 men',
        'EMA_ENG': '13 orphans (13 wait)',
    },
    '小四喜': {
        'DEFAULT': 'Shousuushi',
        'EMA_ENG': 'Little 4 winds',
    },
    '字一色': {
        'DEFAULT': 'Tsuuiisou',
        'EMA_ENG': 'All honours',
    },
    '三槓子': {
        'DEFAULT': 'Sankantsu',
        'EMA_ENG': '3 kongs',
    },
    '四暗刻單騎': {
        'DEFAULT': 'Suuankou tanki',
        'EMA_ENG': '4 concealed pungs (single wait)',
    },
    '四暗刻単騎': {
        'DEFAULT': 'Suuankou tanki',
        'EMA_ENG': '4 concealed pungs (single wait)',
    },
    '綠一色': {
        'DEFAULT': 'Ryuuiisou',
        'EMA_ENG': 'All green',
    },
    '緑一色': {
        'DEFAULT': 'Ryuuiisou',
        'EMA_ENG': 'All green',
    },
    '九蓮宝燈': {
        'DEFAULT': 'Chuuren poutou',
        'EMA_ENG': '9 gates',
    },
    '九蓮寶燈': {
        'DEFAULT': 'Chuuren poutou',
        'EMA_ENG': '9 gates',
    },
    '純正九蓮宝燈': {
        'DEFAULT': 'Junsei chuuren poutou',
        'EMA_ENG': '9 gates (9 wait)',
    },
    '純正九蓮寶燈': {
        'DEFAULT': 'Junsei chuuren poutou',
        'EMA_ENG': '9 gates (9 wait)',
    },
    '清老頭': {
        'DEFAULT': 'Chinroutou',
        'EMA_ENG': 'All terminals',
    },
    '地和': {
        'DEFAULT': 'Chiihou',
        'EMA_ENG': 'Earth\'s blessing',
    },
    '大四喜': {
        'DEFAULT': 'Daisuushi',
        'EMA_ENG': 'Big 4 winds',
    },
    '天和': {
        'DEFAULT': 'Tenhou',
        'EMA_ENG': 'Heaven\'s blessing',
    },
    '人和': {
        'DEFAULT': 'Renhou',
        'EMA_ENG': 'Man\'s blessing',
    },
    '数え役満': {
        'DEFAULT': 'Kazoe-yakuman',
        'EMA_ENG': 'Counted yakuman',
    },
    '滿貫': {
        'DEFAULT': 'Mangan',
    },
    '跳滿': {
        'DEFAULT': 'Haneman',
    },
    '倍滿': {
        'DEFAULT': 'Baiman',
    },
    '三倍滿': {
        'DEFAULT': 'Sanbaiman',
    },
    '役滿': {
        'DEFAULT': 'Yakuman',
    },
    '点∀': {
        'DEFAULT': 'Points ∀',
        'EMA_ENG': 'Points each',
    },

    // Main

    'オンライン対戦麻雀 天鳳 / ランキング': {
        'DEFAULT': 'Tenhou / Rankings',
        'ENG': 'Tenhou / Monthly statistics',
    },
    'オンライン対戦麻雀 天鳳 / 役満': {
        'DEFAULT': 'Tenhou / Yakuman',
        'ENG': 'Tenhou / Yakuman this month',
    },
    'オンライン対戦麻雀 天鳳 / 接続人数': {
        'DEFAULT': 'Tenhou / Connections',
        'ENG': 'Tenhou / Number of simultaneous users each hour this month',
    },
    '天鳳 / Web版': {
        'DEFAULT': 'Tenhou / Web version',
    },
    '【イベント告知】': {
        'DEFAULT': '<Event Notices>',
    },
    'お試しゲストログイン': {
        'DEFAULT': 'Guest Login',
    },
    '新規ID登録': {
        'DEFAULT': 'New ID',
    },
    'IDで続きから': {
        'DEFAULT': 'Existing ID',
    },
    '【入力方法の調整を行っています】': {
        'DEFAULT': 'Input method has been successfully changed',
    },
    '希望の入力方法ではない場合には': {
        'DEFAULT': 'If your desired input method is not chosen, ',
    },
    '「設定」から変更をお願いいたします。': {
        'DEFAULT': 'you can change it via settings.',
    },
    '新規ID': {
        'DEFAULT': 'New ID',
    },
    'フリテン': {
        'DEFAULT': 'Furiten',
    },
    '男': {
        'DEFAULT': 'Male',
    },
    '女': {
        'DEFAULT': 'Female',
    },
    'ID変更': {
        'DEFAULT': 'Change ID',
    },
    '設定': {
        'DEFAULT': 'Settings',
    },
    'ロビーの移動': {
        'DEFAULT': 'Change lobby',
    },
    'このプレーヤ名で新しいIDを作成しますか？': {
        'DEFAULT': 'Would you like to create a new ID with this player name?',
    },
    '●アドレスバーを小さくするには下にスクロールしてからゆっくり上にスクロールします(機種依存あり)●OK/パス/ツモ切りは右クリックまたは余白をダブルタップ': {
        'DEFAULT': 'To make the address bar smaller, scroll down, then slowly scroll up (depends on the device). Right click or double tap to trigger Confirm / Pass / Auto Discard.',
    },
    '再接続しますか？': {
        'DEFAULT': 'Reconnect?',
    },
    'Wi-Fi(無線LAN)やbluetoothは電子レンジや近隣利用者の影響を受け接続が切れる場合があります': {
        'DEFAULT': 'The connection may be broken due to interference of microwave ovens or neighboring users',
    },
    '入力してあるIDをクリアしますがよろしいですか？IDを紛失しないようにコピーしてください。': {
        'DEFAULT': 'Clear the login ID field? Please consider keeping a copy of the ID to avoid losing it',
    },
    '※アプリ版以外で作成したIDも使用可能です。': {
        'DEFAULT': '* IDs created with application version can also be used',
    },
    'プレーヤ名を8文字以内で入力してください': {
        'DEFAULT': 'Please enter player name up to 8 characters',
    },
    'IDが正しくありません': {
        'DEFAULT': 'Incorrect ID',
    },
    'プレーヤID(半角19文字)を入力してください': {
        'DEFAULT': 'Please enter Player ID (19 half-width characters)',
    },
    '登録が完了しました。IDを紛失しないようにコピーしてください。': {
        'DEFAULT': 'Registration has been completed. Please copy the ID so that it will not be lost.',
    },
    '(※180日以上対戦を行っていないIDは削除されますのでご注意ください)': {
        'DEFAULT': '(Please be aware that IDs not playing for 180 days or more will be deleted)',
    },
    '登録に失敗しまし': {
        'DEFAULT': 'Registration unsuccessful',
    },
    'この接続元からは一定期間アクセスできません': {
        'DEFAULT': 'This connection source is temporarily blocked by our server,',
    },
    '多くのプレーヤから不正/迷惑行為の通報が寄せられた可能性があります。': {
        'DEFAULT': 'possibly due to player violation or reports of causing disturbance.',
    },
    'アクセス解除は問い合わせを頂いても行なえない場合があります。': {
        'DEFAULT': 'Unblocking is not always possible even if you submit a request.',
    },
    '健全なコミュニティの運営に何卒ご理解ご協力をいただきますよう': {
        'DEFAULT': 'We appreciate your understanding and cooperation in the efforts of maintaining a healthy community',
    },
    'よろしくお願い申し上げます': {
        'DEFAULT': 'Thank you for your consideration.',
    },
    '通報が完了しました': {
        'DEFAULT': 'Report submitted',
    },
    'この機能は個室では利用できません': {
        'DEFAULT': 'This feature is unavaliable in private lobbies',
    },
    'ランキング戦ロビーに移動してください': {
        'DEFAULT': 'Please use the public lobby instead',
    },
    '通報に失敗しました': {
        'DEFAULT': 'Report not submitted',
    },
    '観戦可能な対戦は現在ありません': {
        'DEFAULT': 'Spectating is currently not available yet',
    },
    '観戦情報が見つかりませんでした': {
        'DEFAULT': 'Unable to view game info',
    },
    'この対戦は既に終了している可能性があります': {
        'DEFAULT': 'The game might still be in progress',
    },
    '大会ロビーの作成が完了しました': {
        'DEFAULT': 'Tournament lobby created',
    },
    '大会ロビーの作成に失敗しました': {
        'DEFAULT': 'Unable to create tournament lobby',
    },
    '予約中は牌譜を閲覧できません': {
        'DEFAULT': 'Unable to view replays while queuing for a game',
    },
    '予約中は観戦できません': {
        'DEFAULT': 'Unable to spectate while queuing for a game',
    },
    '必要な有効期限が不足しています': {
        'DEFAULT': 'A renewal of subscription is required',
    },
    '今すぐ購入しますか？': {
        'DEFAULT': 'Would you like to make a payment?',
    },
    'ID互換のないロビーへは移動できません': {
        'DEFAULT': 'Incompatible Lobby ID',
    },
    '一時的に使用している外部IDの有効期限が切れました': {
        'DEFAULT': 'This external ID is already expired',
    },
    '参加に必要な条件を満たしていません': {
        'DEFAULT': 'You do not meet the requirements for participation',
    },
    '外部IDでは利用できません': {
        'DEFAULT': 'Usage of external ID is not allowed',
    },
    'このロビーでは参加登録は行えません': {
        'DEFAULT': 'Registration cannot be done in this lobby',
    },
    '参加登録が完了しました': {
        'DEFAULT': 'Registration completed',
    },
    'すでに参加登録が完了しています': {
        'DEFAULT': 'Registered for participation has already been completed',
    },
    'このルールへの予約は許可されていません': {
        'DEFAULT': 'Unable to queue game with the selected rule set',
    },
    '段位戦の上級/特上卓では、不正防止のため対戦人数が100人未満のルールを予約するには有料会員の有効期限の残りが91日以上必要です': {
        'DEFAULT': 'In an effort to prevent cheating in Joukyuu and Tokujou, games with less than 100 active players cannot be queued unless you have an active subscription with atleast 91 days remaining.',
    },
    '接続数が8000人以下の場合のみ予約が許可されています': {
        'DEFAULT': 'You may only queue for a game when the number of connections is 8000 or less',
    },
    'トレーニングを使用するにはID登録が必要です。ログイン画面の「新規ID」からIDを取得してください': {
        'DEFAULT': 'Training mode requires a registered ID. Please register a new ID from the login page',
    },
    '牌譜の読み込みに失敗しました。': {
        'DEFAULT': 'Unable to launch the replay',
    },
    'プレイ中の牌譜はゲーム終了後に閲覧可能になります': {
        'DEFAULT': 'If the game is currently in progress, please wait for it to finish',
    },

    // Error messages 1001 ~ 1006, 1019, 1021

    'プレーヤ名が正しくありません。プレーヤ名には使用できない文字があります。(ERR1001)': {
        'DEFAULT': 'The player name is incorrect. There are characters that can not be used for the player name. (ERR 1001)',
    },
    'このプレーヤ名を使用するにはIDで入場する必要があります':{
        'DEFAULT': 'This name is registered. Please use the correct ID to login',
    },
    '■IDの再発行方法は以下を参照してください': {
        'DEFAULT': 'Visit the link below for information on how to reissue an ID',
    },
    '180日以上対戦を行っていないIDは停止または削除されている場合があります。七段以上で有料版の決済情報が確認できる場合にはIDの復元が可能です。お早めにお問い合わせください。(ERR1003)': {
        'DEFAULT': 'IDs not playing for 180 days or more might be deleted. ID can be recovered for paid accounts holding a rank of 7 dan or above after verification of payment details. Please contact us as soon as possible. (ERR1003)',
    },
    'このプレーヤは既に接続済みです。しばらく経ってから接続してください。(ERR1004)': {
        'DEFAULT': 'This account is already logged in. Please try again later. (ERR1004)',
    },
    'このプレーヤは既に登録済みです。同じプレーヤ名は使用できません。(ERR1005)': {
        'DEFAULT': 'This player has already been registered. The same player name can not be used. (ERR 1005)',
    },
    'この接続元からの新規登録は約７日間行なえません(ERR1006)': {
        'DEFAULT': 'New registration from this connection source can not be done for about 7 days (ERR 1006)',
    },
    '大会ロビーが見つかりませんでした。(ERR1019)': {
        'DEFAULT': 'The tournament lobby could not be found (ERR 1019)',
    },
    '外部ログインサーバからの応答がありません(ERR1021)': {
        'DEFAULT': 'There is no response from the login server (ERR 1021)',
    },

    // Settings

    '環境': {
        'DEFAULT': 'Environment',
    },
    '/ 設定': {
        'DEFAULT': '/ Settings',
    },
    '画面方向:回転': {
        'DEFAULT': 'Screen orientation:Rotate',
    },
    '画面方向:Default': {
        'DEFAULT': 'Screen orientation:Default',
    },
    '※アプリ版でのみご利用いただけます': {
        'DEFAULT': '※Only applicable in App version',
    },
    '配信ID保護': {
        'DEFAULT': 'ID protection for live stream',
    },
    '※ログイン画面のID入力を非表示にします': {
        'DEFAULT': '※Makes your ID hidden on the login page',
    },
    '入力補助:Default': {
        'DEFAULT': 'Input Assist: Default',
    },
    '入力補助:3TAP': {
        'DEFAULT': 'Input Assist: 3TAP',
    },
    '入力補助:2TAP': {
        'DEFAULT': 'Input Assist: 2TAP',
    },
    '※縦画面のみで表示されます': {
        'DEFAULT': '※Only displayed when in portrait view',
    },
    '牌山表示:Default': {
        'DEFAULT': 'Show Wall:Default',
    },
    '牌山表示:あり': {
        'DEFAULT': 'Show Wall:On',
    },
    '牌山表示:なし': {
        'DEFAULT': 'Show Wall:Off',
    },
    'SEなし': {
        'DEFAULT': 'No SE',
    },
    '卓': {
        'DEFAULT': 'Table',
    },
    '標準の画像': {
        'DEFAULT': 'Use default BG image',
    },
    '画像URL:': {
        'DEFAULT': 'Image URL:',
    },
    '牌': {
        'DEFAULT': 'Tile',
    },
    '牌背色:': {
        'DEFAULT': 'Tile Colour',
    },
    'あり': {
        'DEFAULT': 'On',
    },
    'なし': {
        'DEFAULT': 'Off',
    },
    '※「鳴きなし」他が常時手牌下に表示されます': {
        'DEFAULT': 'On: on-screen beneath your tiles; Off: appear on button press',
    },
    'Desktop入力:Default': {
        'DEFAULT': 'In-game options: Default',
    },
    'Desktop入力:あり': {
        'DEFAULT': 'In-game options: On-screen',
    },
    'Desktop入力:なし': {
        'DEFAULT': 'In-game options: From button',
    },

    // Practice play game modes

    '四般東喰赤速': {
        'DEFAULT': '4p tonpusen fast',
        'EMA_ENG': '4p E-only fast',
    },
    '(４人打 東風 喰断アリ 赤アリ 速)': {
        'DEFAULT': '4p tonpusen, fast, play against 3 tsumobots',
        'EMA_ENG': '4p East only, fast, play against 3 bots (bots don\'t try to win)',
    },
    '四般東喰赤': {
        'DEFAULT': '4p tonpusen',
        'EMA_ENG': '4p E-only',
    },
    '(４人打 東風 喰断アリ 赤アリ)': {
        'DEFAULT': '4p tonpusen, play against 3 tsumobots',
        'EMA_ENG': '4p East only, play against 3 bots',
    },
    '三般東喰赤': {
        'DEFAULT': '3p tonpusen',
        'EMA_ENG': '3p E-only',
    },
    '(３人打 東風 喰断アリ 赤アリ)': {
        'DEFAULT': '3p tonpusen, play against 2 tsumobots',
        'EMA_ENG': '3p East only, play against 2 bots',
    },
    '四若東速祝５': {
        'DEFAULT': '4p tonpusen fast + shuugi',
        'EMA_ENG': '4p E-only fast + shuugi',
    },
    '(４人打 東風 喰断アリ 赤アリ 祝儀)': {
        'DEFAULT': '4p tonpusen, fast, play against 3 tsumobots, shuugi',
        'EMA_ENG': '4p East only, fast, 3 bots, with parlor bonuses (shuugi)',
    },
    '三若東速祝５': {
        'DEFAULT': '3p tonpusen fast + shuugi ',
        'EMA_ENG': '3p E-only fast + shuugi ',
    },
    '(３人打 東風 喰断アリ 赤アリ 祝儀)': {
        'DEFAULT': '3p tonpusen, fast play against 2 tsumobots, shuugi',
        'EMA_ENG': '3p East only, fast play against 2 bots, shuugi',
    },
    '※喰断ナシON/OFF': {
        'DEFAULT': '※Show / hide kuitan games',
        'ENG': '※Show / hide additional modes',
    },

    // Game rules

    '東風+4局サドンデス': {
        'DEFAULT': 'Tonpusen + 4 rounds of sudden death',
        'EMA_ENG': 'East-only + 4 rounds of sudden death',
        'ENG': 'East',
    },
    '東南+4局サドンデス': {
        'DEFAULT': 'Hanchan + 4 rounds of sudden death',
        'EMA_ENG': 'East & South + 4 rounds of sudden death',
        'ENG': 'East & South',
    },
    'ウマ': {
        'DEFAULT': 'Uma',
        'ENG': 'Placement bonus',
    },
    '1本場': {
        'DEFAULT': 'Honba',
        'ENG': 'continuation',
    },
    '後': {
        'DEFAULT': 'after',
        'ENG': 'after discard',
    },
    '明槓ドラ': {
        'DEFAULT': 'Mindora',
        'ENG': 'Reveal dora on open kan',
    },
    '喰断': {
        'DEFAULT': 'Kuitan',
        'ENG': 'Open all-simples',
    },
    '東西場': {
        'DEFAULT': 'Tonshaba',
        'ENG': 'E-W round seat',
    },
    '和了止め': {
        'DEFAULT': 'Agariyame',
        'ENG': 'End on dealer win',
    },
    '聴牌止め': {
        'DEFAULT': 'Tenpaiyame',
        'ENG': 'End on dealer ready',
    },

    // Lobby and game

    '収支': {
        'DEFAULT': 'Income',
        'ENG': 'Parlor income',
    },
    '祝儀': {
        'DEFAULT': 'Shuugi',
        'ENG': 'Parlor bonuses',
    },
    '平均': {
        'DEFAULT': 'Average',
        'ENG': 'Average',
    },
    '平均収支': {
        'DEFAULT': 'Avg income',
        'ENG': 'Avg parlor income',
    },
    '平均祝儀': {
        'DEFAULT': 'Avg shuugi',
        'ENG': 'Avg parlor bonuses',
    },
    '視': {
        'DEFAULT': 'View pt.',
    },
    '局': {
        'DEFAULT': 'Round',
    },
    '巡': {
        'DEFAULT': 'Turn',
    },
    'ランキング戦': {
        'DEFAULT': 'Ranked Play',
    },
    '(段位戦、雀荘戦)': {
        'DEFAULT': '(Ranked, Parlor)',
    },
    'イベント会場１': {
        'DEFAULT': 'Event Venue 1',
    },
    '(公式イベントで使用します)': {
        'DEFAULT': '(Official events)',
    },
    'イベント会場２': {
        'DEFAULT': 'Event Venue 2',
    },
    'その他': {
        'DEFAULT': 'Others',
    },
    '(個室番号を指定してロビーを移動します)': {
        'DEFAULT': '(Go to a private lobby)',
    },
    'ロビーの新規作成': {
        'DEFAULT': 'Create lobby',
    },
    '(天鳳サイトの個室作成ページを開きます)': {
        'DEFAULT': '(Link to the lobby creation page)',
    },
    '※下の«»でタブを移動してください。': {
        'DEFAULT': 'Use the «» buttons below to navigate',
    },
    '接続:': {
        'DEFAULT': 'Online:',
    },
    '待機:': {
        'DEFAULT': 'Idle:',
    },
    '終局:': {
        'DEFAULT': 'Last:',
    },
    '有効期限:': {
        'DEFAULT': 'Expire: ',
    },
    '段級位を取得するには': {
        'DEFAULT': 'Please register an ID in order',
    },
    '「新規ID」登録が必要です': {
        'DEFAULT': 'to be qualified for rankings',
    },
    'テストプレイ': {
        'DEFAULT': 'Practice play against the computer',
    },
    '対戦': {
        'DEFAULT': 'Play',
    },
    '予約': {
        'DEFAULT': 'Queue',
    },
    '1位': {
        'DEFAULT': '1st',
    },
    '2位': {
        'DEFAULT': '2nd',
    },
    '3位': {
        'DEFAULT': '3rd',
    },
    '4位': {
        'DEFAULT': '4th',
    },
    '四麻:': {
        'DEFAULT': '4P',
    },
    '三麻:': {
        'DEFAULT': '3P',
    },
    '月間戦績': {
        'DEFAULT': 'Monthly Stats',
    },
    '通算': {
        'DEFAULT': 'Raw',
    },
    '得点': {
        'DEFAULT': 'Points',
    },
    '順位': {
        'DEFAULT': 'Placement',
    },
    '連対率': {
        'DEFAULT': 'Rentai',
        'EMA_ENG': '1st & 2nd',
    },
    'トップ': {
        'DEFAULT': 'Top rate',
        'EMA_ENG': '1st rate',
    },
    'ラス率': {
        'DEFAULT': 'Last rate',
        'EMA_ENG': '4th rate',
    },
    '総合': {
        'DEFAULT': 'Total',
    },
    '※段位戦４人打ち合算戦績': {
        'DEFAULT': '※Summary of ranked 4P games',
    },
    '※段位戦３人打ち合算戦績': {
        'DEFAULT': '※Summary of ranked 3P games',
    },
    '※表示する対戦ルールを選択してください': {
        'DEFAULT': 'Choose a game type to display its stats',
    },
    '通算戦績': {
        'DEFAULT': 'Raw Stats',
    },
    '１位率': {
        'DEFAULT': '1st',
    },
    '２位率': {
        'DEFAULT': '2nd',
    },
    '３位率': {
        'DEFAULT': '3rd',
    },
    '４位率': {
        'DEFAULT': '4th',
    },
    '飛び率': {
        'DEFAULT': 'Tobi',
        'EMA_ENG': 'Busted',
    },
    '対戦数': {
        'DEFAULT': '# of games',
    },
    '平均得点': {
        'DEFAULT': 'Avg points',
    },
    '平均順位': {
        'DEFAULT': 'Avg placing',
    },
    '和了率': {
        'DEFAULT': 'Win rate',
    },
    '放銃率': {
        'DEFAULT': 'Deal-in rate',
    },
    '副露率': {
        'DEFAULT': 'Call rate',
    },
    '立直率': {
        'DEFAULT': 'Riichi rate',
    },
    '牌譜': {
        'DEFAULT': 'Replay',
    },
    '牌譜 |': {
        'DEFAULT': 'Replay |',
    },
    '観戦': {
        'DEFAULT': 'Spectate',
    },
    '観戦 |': {
        'DEFAULT': 'Watch |',
    },
    'ヘルプ': {
        'DEFAULT': 'Help',
    },
    '牌理': {
        'DEFAULT': 'Analyser',
    },
    '料金のお支払い': {
        'DEFAULT': 'Pay membership fee',
    },
    '牌譜/観戦URLを入力してください': {
        'DEFAULT': 'Enter URL of replay or live game',
    },
    'ログイン画面の「新規ID」からIDを取得してください': {
        'DEFAULT': 'Please first register an ID from the login page',
    },
    '●「長押し」または「右クリック」から別タブで開く/URLのコピーなどが行えます': {
        'DEFAULT': '● Long press or right click to bring up the context menu.',
    },
    '●観戦は５分遅れ': {
        'DEFAULT': '● Games are delayed by five minutes in spectating mode.',
    },
    'すべての牌譜一覧を表示': {
        'DEFAULT': 'List all replays',
    },
    'この端末に記録されている牌譜を表示': {
        'DEFAULT': 'List replays on this device',
    },
    '※有料会員の状態で打った牌譜のみ検索できます': {
        'DEFAULT': '※Subscribed users can access all replays from any device',
    },
    '※１０日前までの牌譜が検索可能です': {
        'DEFAULT': '※Replays will be kept for a period of 10 days',
    },
    '牌譜の検索やダウンロードはこちらから行えます': {
        'DEFAULT': 'Find and download game replays here',
    },
    '観戦可能な対戦はありません': {
        'DEFAULT': 'There are no matches that can be watched.',
    },
    '画像や音声がロードされていて正常に描画/再生可能かを確認するための機能です。もし異常があればゲームの進行に支障が出る場合がありますので、このページをリロードしてください。': {
        'DEFAULT': 'You may use this feature to test whether image and sound files can be properly loaded. If there exists any abnormality which may hinder the progress of the game, please refresh this page.',
    },
    '上級卓の入場条件(１級以上または有効期限60日以上)を満たしていません※七段R2000以上は入場できません': {
        'DEFAULT': 'Must be at least 1 Kyuu to enter Joukyuu (Lower dan). ※Not available after 7 dan + R2000',
    },
    '特上卓の入場条件(四段R1800以上を満たしていません': {
        'DEFAULT': 'Must be at least 4 dan + R1800 to enter Tokujou (Upper dan).',
    },
    '雀荘戦の入場条件(四段R1800以上の有料会員)を満たしていません': {
        'DEFAULT': 'Must be at least 4 dan + R1800 to enter Janso (Parlor). Only available with subscription.',
    },
    '鳳凰卓の入場条件(七段R2000以上の有料会員)を満たしていません': {
        'DEFAULT': 'Must be at least 7 dan + R2000 to enter Houou (Phoenix). Only available with subscription.',
    },
    '上にゆっくりスクロールしてください': {
        'DEFAULT': 'Scroll down slowly until this message disappears',
    },
    'ツモ': {
        'DEFAULT': 'Tsumo',
    },
    'ロン': {
        'DEFAULT': 'Ron',
    },
    'パオ': {
        'DEFAULT': 'Pao',
    },
    'リーチ': {
        'DEFAULT': 'Riichi',
    },
    '流局': {
        'DEFAULT': 'Ryuukyoku',
        'EMA_ENG': 'Redeal',
    },
    '四家立直': {
        'DEFAULT': 'Suucha riichi',
        'EMA_ENG': 'Redeal: Quadruple riichi',
    },
    '三家和了': {
        'DEFAULT': 'Sancha hou',
        'EMA_ENG': 'Redeal: Triple ron',
    },
    '四風連打': {
        'DEFAULT': 'Suufon renda',
        'EMA_ENG': 'Redeal: 4 identical winds discarded',
    },
    '四槓散了': {
        'DEFAULT': 'Suukaikan',
        'EMA_ENG': 'Redeal: Four kongs',
    },
    '九種九牌': {
        'DEFAULT': 'Kyuushuu kyuuhai',
        'EMA_ENG': 'Redeal: 9 terminals & honours',
    },
    'カン': {
        'DEFAULT': 'Kan',
    },
    '終了': {
        'DEFAULT': 'Quit',
    },
    '對局': {
        'DEFAULT': 'Start',
    },
    '不聴宣言': {
        'DEFAULT': 'Noten',
    },
    ' 鳴き': {
        'DEFAULT': 'Call',
    },
    'ポン': {
        'DEFAULT': 'Pon',
        'EMA_ENG': 'Pung',
    },
    'チー': {
        'DEFAULT': 'Chii',
        'EMA_ENG': 'Chow',
    },
    '槓': {
        'DEFAULT': 'Kan',
        'EMA_ENG': 'Kong',
    },
    'キタ': {
        'DEFAULT': 'Kita',
    },
    '終局': {
        'DEFAULT': 'End',
    },
    '飜': {
        'DEFAULT': 'Han',
    },
    '符': {
        'DEFAULT': 'Fu',
    },
    '点': {
        'DEFAULT': 'Points',
    },
    'ドラ': {
        'DEFAULT': 'Dora',
    },
    '待ち': {
        'DEFAULT': 'Waits',
    },
    '手牌': {
        'DEFAULT': 'Hand',
    },
    '牌山': {
        'DEFAULT': 'Wall',
    },
    '(匿名表示 ON)': {
        'DEFAULT': '(ID Display ON)',
    },
    '(匿名表示 OFF)': {
        'DEFAULT': '(ID Display OFF)',
    },
    'ランキング': {
        'DEFAULT': 'Summary stats',
    },
    '接続人数': {
        'DEFAULT': 'Players by hour (JST: UTC+9)',
    },
    '※最大同時接続人数': {
        'DEFAULT': 'Maximum number of simultaneous users',
    },
    '拔き': {
        'DEFAULT': 'Nuki',
        'ENG': 'Melded North dora',
    },
    '役': {
        'DEFAULT': 'Yaku',
    },

    // Double yakuman (incomplete)

    '小四喜 字一色': {
        'DEFAULT': 'Shousuushi + Tsuuiisou',
        'EMA_ENG': 'Little 4 winds + All honours',
    },
    '大四喜 字一色': {
        'DEFAULT': 'Daisuushi + Tsuuiisou',
        'EMA_ENG': 'Big 4 winds + All honours',
    },
    '大三元 四暗刻': {
        'DEFAULT': 'Daisangen + Suuankou',
        'EMA_ENG': '3 dragons + 4 concealed pungs',
    },
    '大三元 字一色': {
        'DEFAULT': 'Daisangen + Tsuuiisou',
        'EMA_ENG': '3 dragons + All honours',
    },
    '小四喜 四暗刻単騎': {
        'DEFAULT': 'Shousuushi + Suuankou tanki',
        'EMA_ENG': 'Little 4 winds + 4 concealed pungs (single wait)',
    },
    '四暗刻単騎 清老頭': {
        'DEFAULT': 'Suuankou tanki + Chinroutou',
        'EMA_ENG': '4 concealed pungs (single wait) + All terminals',
    },
    '四暗刻 緑一色': {
        'DEFAULT': 'Suuankou tanki + Ryuuiisou',
        'EMA_ENG': '4 concealed pungs + All green',
    },
    '小四喜 四暗刻': {
        'DEFAULT': 'Shousuushi + Suuankou',
        'EMA_ENG': 'Little 4 winds + 4 concealed pungs',
    },


    // replays

    '天鳳牌譜再生': {
        'DEFAULT': 'Game replay',
    },
    '牌 譜 を 再 生': {
        'DEFAULT': 'Start the replay',
    },
    '前局': {
        'DEFAULT': 'Prev. hand',
    },
    '次局': {
        'DEFAULT': 'Next hand',
    },
    '上家': {
        'DEFAULT': '↺',
    },
    '下家': {
        'DEFAULT': '↻',
    },
    '手牌 OFF': {
        'DEFAULT': 'Show only front hand.',
    },
    '手牌 ON': {
        'DEFAULT': 'Show all hands.',
    },
    '暗転 OFF': {
        'DEFAULT': 'Don\'t shade discarded drawn tiles.',
    },
    '暗転 ON': {
        'DEFAULT': 'Shade discarded drawn tiles.',
    },
    '- 右クリック … 進む': {
        'DEFAULT': '- Right click: Step forward',
    },
    '- 左クリック … 戻る': {
        'DEFAULT': '- Left click: Step backward',
    },
    '- ボタン長押し … オートリピート': {
        'DEFAULT': '- Long press: Step continuously',
    },
    '- センターパネル … 得点差表示': {
        'DEFAULT': '- Center Panel: Display scores',
    },

    // promotion

    '認定': {
        'DEFAULT': 'Acknowledgement',
    },
    '認定段位/級位    :    ９級': {
        'DEFAULT': 'You have been promoted to: 9 kyuu',
    },
    '認定段位/級位    :    ８級': {
        'DEFAULT': 'You have been promoted to: 8 kyuu',
    },
    '認定段位/級位    :    ７級': {
        'DEFAULT': 'You have been promoted to: 7 kyuu',
    },
    '認定段位/級位    :    ６級': {
        'DEFAULT': 'You have been promoted to: 6 kyuu',
    },
    '認定段位/級位    :    ５級': {
        'DEFAULT': 'You have been promoted to: 5 kyuu',
    },
    '認定段位/級位    :    ４級': {
        'DEFAULT': 'You have been promoted to: 4 kyuu',
    },
    '認定段位/級位    :    ３級': {
        'DEFAULT': 'You have been promoted to: 3 kyuu',
    },
    '認定段位/級位    :    ２級': {
        'DEFAULT': 'You have been promoted to: 2 kyuu',
    },
    '認定段位/級位    :    １級': {
        'DEFAULT': 'You have been promoted to: 1 kyuu',
    },
    '認定段位/級位    :    初段': {
        'DEFAULT': 'You have been promoted to: 1st dan',
    },
    '認定段位/級位    :    二段': {
        'DEFAULT': 'You have been promoted to: 2nd dan',
    },
    '認定段位/級位    :    三段': {
        'DEFAULT': 'You have been promoted to: 3rd dan',
    },
    '認定段位/級位    :    四段': {
        'DEFAULT': 'You have been promoted to: 4th dan',
    },
    '認定段位/級位    :    五段': {
        'DEFAULT': 'You have been promoted to: 5th dan',
    },
    '認定段位/級位    :    六段': {
        'DEFAULT': 'You have been promoted to: 6th dan',
    },
    '認定段位/級位    :    七段': {
        'DEFAULT': 'You have been promoted to: 7th dan',
    },
    '認定段位/級位    :    八段': {
        'DEFAULT': 'You have been promoted to: 8th dan',
    },
    '認定段位/級位    :    九段': {
        'DEFAULT': 'You have been promoted to: 9th dan',
    },
    '認定段位/級位    :    十段': {
        'DEFAULT': 'You have been promoted to: 10th dan',
    },
    '認定段位/級位    :    天鳳位': {
        'DEFAULT': 'You have been promoted to: Tenhoui',
        'EMA_ENG': 'You have been promoted to: Tenhou Immortal',
    },

    // 3 player mahjong
    '認定段位/級位    :    サンマ ９級': {
        'DEFAULT': 'You have been promoted to: 9 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ８級': {
        'DEFAULT': 'You have been promoted to: 8 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ７級': {
        'DEFAULT': 'You have been promoted to: 7 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ６級': {
        'DEFAULT': 'You have been promoted to: 6 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ５級': {
        'DEFAULT': 'You have been promoted to: 5 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ４級': {
        'DEFAULT': 'You have been promoted to: 4 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ３級': {
        'DEFAULT': 'You have been promoted to: 3 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ ２級': {
        'DEFAULT': 'You have been promoted to: 2 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ １級': {
        'DEFAULT': 'You have been promoted to: 1 kyuu (3p)',
    },
    '認定段位/級位    :    サンマ 初段': {
        'DEFAULT': 'You have been promoted to: 1st dan (3p)',
    },
    '認定段位/級位    :    サンマ 二段': {
        'DEFAULT': 'You have been promoted to: 2nd dan (3p)',
    },
    '認定段位/級位    :    サンマ 三段': {
        'DEFAULT': 'You have been promoted to: 3rd dan (3p)',
    },
    '認定段位/級位    :    サンマ 四段': {
        'DEFAULT': 'You have been promoted to: 4th dan (3p)',
    },
    '認定段位/級位    :    サンマ 五段': {
        'DEFAULT': 'You have been promoted to: 5th dan (3p)',
    },
    '認定段位/級位    :    サンマ 六段': {
        'DEFAULT': 'You have been promoted to: 6th dan (3p)',
    },
    '認定段位/級位    :    サンマ 七段': {
        'DEFAULT': 'You have been promoted to: 7th dan (3p)',
    },
    '認定段位/級位    :    サンマ 八段': {
        'DEFAULT': 'You have been promoted to: 8th dan (3p)',
    },
    '認定段位/級位    :    サンマ 九段': {
        'DEFAULT': 'You have been promoted to: 9th dan (3p)',
    },
    '認定段位/級位    :    サンマ 十段': {
        'DEFAULT': 'You have been promoted to: 10th dan (3p)',
    },
    '認定段位/級位    :    サンマ 天鳳位': {
        'DEFAULT': 'You have been promoted to: Tenhoui (3p)',
        'EMA_ENG': 'You have been promoted to: Tenhou Immortal (3p)',
    },
    '貴殿は天鳳において卓越した技能を遺憾なく発揮され優秀な': {
        'DEFAULT': 'You have unquestionably demonstrated outstanding skills and have achieved excellent results.',
    },
    '成績をおさめられました。今後もさらなる雀力向上に精進され': {
        'DEFAULT': 'With this promotion, we acknowledge your advancement,',
    },
    'ますようここに段位/級位を認定し栄誉を称えます。': {
        'DEFAULT': 'and wish you success for future promotions.',
    },
    '天鳳段位認定協会': {
        'DEFAULT': 'Tenhou Rankings Authority',
    },
    '天鳳サンマ漁業協会': {
        'DEFAULT': 'Tenhou Sanma Fishery Association',
    },
};


const partialTranslation = {
    '鳴きなし': {
        'DEFAULT': 'No call',
    },
    '自動理牌': {
        'DEFAULT': 'Sort',
    },
    '自動和了': {
        'DEFAULT': 'Auto ron',
    },
    'ツモ切り': {
        'DEFAULT': 'Auto discard',
    },
    '四般東喰赤速': {
        'DEFAULT': '4 Ippan Tonpu Fast',
        'EMA_ENG': '4p Novice E Fast',
        'ENG': '4p Novice E Fast',
    },
    '四般東喰赤': {
        'DEFAULT': '4 Ippan Tonpu',
        'EMA_ENG': '4p Novice E',
        'ENG': '4p Novice E',
    },
    '四般東喰': {
        'DEFAULT': '4 Ippan Tonpu Akanashi',
        'EMA_ENG': '4p Novice E No-red-5',
        'ENG': '4p Novice E No-red-5',
    },
    '四般東': {
        'DEFAULT': '4 Ippan Tonpu Nashinashi',
        'EMA_ENG': '4p Novice E No-red-5 No-open-all-simples',
        'ENG': '4p Novice E No-red-5 No-open-all-simples',
    },
    '四般南喰赤速': {
        'DEFAULT': '4 Ippan Hanchan Fast',
        'EMA_ENG': '4p Novice E+S Fast',
        'ENG': '4p Novice E+S Fast',
    },
    '四般南喰赤': {
        'DEFAULT': '4 Ippan Hanchan',
        'EMA_ENG': '4p Novice E+S',
        'ENG': '4p Novice E+S',
    },
    '四般南喰': {
        'DEFAULT': '4 Ippan Hanchan Akanashi',
        'EMA_ENG': '4p Novice E+S No-red-5',
        'ENG': '4p Novice E+S No-red-5',
    },
    '四般南': {
        'DEFAULT': '4 Ippan Hanchan Nashinashi',
        'EMA_ENG': '4p Novice E+S No-red-5 No-open-all-simples',
        'ENG': '4p Novice E+S No-red-5 No-open-all-simples',
    },
    '三般東喰赤速': {
        'DEFAULT': '3 Ippan Tonpu Fast',
        'EMA_ENG': '3p Novice E Fast',
        'ENG': '3p Novice E Fast',
    },
    '三般東喰赤': {
        'DEFAULT': '3 Ippan Tonpu',
        'EMA_ENG': '3p Novice E',
        'ENG': '3p Novice E',
    },
    '三般東喰': {
        'DEFAULT': '3 Ippan Tonpu Akanashi',
        'EMA_ENG': '3p Novice E No-red-5',
        'ENG': '3p Novice E No-red-5',
    },
    '三般東': {
        'DEFAULT': '3 Ippan Tonpu Nashinashi',
        'EMA_ENG': '3p Novice E No-red-5 No-open-all-simples',
        'ENG': '3p Novice E No-red-5 No-open-all-simples',
    },
    '三般南喰赤速': {
        'DEFAULT': '3 Ippan Hanchan Fast',
        'EMA_ENG': '3p Novice E+S Fast',
        'ENG': '3p Novice E+S Fast',
    },
    '三般南喰赤': {
        'DEFAULT': '3 Ippan Hanchan',
        'EMA_ENG': '3p Novice E+S',
        'ENG': '3p Novice E+S',
    },
    '三般南喰': {
        'DEFAULT': '3 Ippan Hanchan Akanashi',
        'EMA_ENG': '3p Novice E No-red-5',
        'ENG': '3p Novice E No-red-5',
    },
    '三般南': {
        'DEFAULT': '3 Ippan Hanchan Nashinashi',
        'EMA_ENG': '3p Novice E+S No-red-5 No-open-all-simples',
        'ENG': '3p Novice E+S No-red-5 No-open-all-simples',
    },
    '四上東喰赤速': {
        'DEFAULT': '4 Joukyuu Tonpu Fast',
        'EMA_ENG': '4p Lower-dan E Fast',
        'ENG': '4p 1k+ E Fast',
    },
    '四上東喰赤': {
        'DEFAULT': '4 Joukyuu Tonpu',
        'EMA_ENG': '4p Lower-dan E',
        'ENG': '4p 1k+ E',
    },
    '四上東喰': {
        'DEFAULT': '4 Joukyuu Tonpu Akanashi',
        'EMA_ENG': '4p Lower-dan E No-red-5',
        'ENG': '4p 1k+ E No-red-5',
    },
    '四上東': {
        'DEFAULT': '4 Joukyuu Tonpu Nashinashi',
        'EMA_ENG': '4p Lower-dan E No-red-5 No-open-all-simples',
        'ENG': '4p 1k+ E No-red-5 No-open-all-simples',
    },
    '四上南喰赤速': {
        'DEFAULT': '4 Joukyuu Hanchan Fast',
        'EMA_ENG': '4p Lower-dan E+S Fast',
        'ENG': '4p 1k+ E+S Fast',
    },
    '四上南喰赤': {
        'DEFAULT': '4 Joukyuu Hanchan',
        'EMA_ENG': '4p Lower-dan E+S',
        'ENG': '4p 1k+ E+S',
    },
    '四上南喰': {
        'DEFAULT': '4 Joukyuu Hanchan Akanashi',
        'EMA_ENG': '4p Lower-dan E+S No-red-5',
        'ENG': '4p 1k+ E+S No-red-5',
    },
    '四上南': {
        'DEFAULT': '4 Joukyuu Hanchan Nashinashi',
        'EMA_ENG': '4p Lower-dan E+S No-red-5 No-open-all-simples',
        'ENG': '4p 1k+ E+S No-red-5 No-open-all-simples',
    },
    '三上東喰赤速': {
        'DEFAULT': '3 Joukyuu Tonpu Fast',
        'EMA_ENG': '3p Lower-dan E Fast',
        'ENG': '3p 1k+ E Fast',
    },
    '三上東喰赤': {
        'DEFAULT': '3 Joukyuu Tonpu',
        'EMA_ENG': '3p Lower-dan E',
        'ENG': '3p 1k+ E',
    },
    '三上東喰': {
        'DEFAULT': '3 Joukyuu Tonpu Akanashi',
        'EMA_ENG': '3p Lower-dan E No-red-5',
        'ENG': '3p 1k+ E No-red-5',
    },
    '三上東': {
        'DEFAULT': '3 Joukyuu Tonpu Nashinashi',
        'EMA_ENG': '3p Lower-dan E No-red-5 No-open-all-simples',
        'ENG': '3p 1k+ E No-red-5 No-open-all-simples',
    },
    '三上南喰赤速': {
        'DEFAULT': '3 Joukyuu Hanchan Fast',
        'EMA_ENG': '3p Lower-dan E+S Fast',
        'ENG': '3p 1k+ E+S Fast',
    },
    '三上南喰赤': {
        'DEFAULT': '3 Joukyuu Hanchan',
        'EMA_ENG': '3p Lower-dan E+S',
        'ENG': '3p 1k+ E+S',
    },
    '三上南喰': {
        'DEFAULT': '3 Joukyuu Hanchan Akanashi',
        'EMA_ENG': '3p Lower-dan E+S No-red-5',
        'ENG': '3p 1k+ E+S No-red-5',
    },
    '三上南': {
        'DEFAULT': '3 Joukyuu Hanchan Nashinashi',
        'EMA_ENG': '3p Lower-dan E+S No-red-5 No-open-all-simples',
        'ENG': '3p 1k+ E+S No-red-5 No-open-all-simples',
    },
    '四特東喰赤速': {
        'DEFAULT': '4 Tokujou Tonpu Fast',
        'EMA_ENG': '4p Upper-dan E Fast',
        'ENG': '4p 4d+R1800+ E Fast',
    },
    '四特東喰赤': {
        'DEFAULT': '4 Tokujou Tonpu',
        'EMA_ENG': '4p Upper-dan E',
        'ENG': '4p 4d+R1800+ E',
    },
    '四特東喰': {
        'DEFAULT': '4 Tokujou Tonpu Akanashi',
        'EMA_ENG': '4p Upper-dan E No-red-5',
        'ENG': '4p 4d+R1800+ E No-red-5',
    },
    '四特東': {
        'DEFAULT': '4 Tokujou Tonpu Nashinashi',
        'EMA_ENG': '4p Upper-dan E No-red-5 No-open-all-simples',
        'ENG': '4p 4d+R1800+ E No-red-5 No-open-all-simples',
    },
    '四特南喰赤速': {
        'DEFAULT': '4 Tokujou Hanchan Fast',
        'EMA_ENG': '4p Upper-dan E+S Fast',
        'ENG': '4p 4d+R1800+ E+S Fast',
    },
    '四特南喰赤': {
        'DEFAULT': '4 Tokujou Hanchan',
        'EMA_ENG': '4p Upper-dan E+S',
        'ENG': '4p 4d+R1800+ E+S',
    },
    '四特南喰': {
        'DEFAULT': '4 Tokujou Hanchan Akanashi',
        'EMA_ENG': '4p Upper-dan E+S No-red-5',
        'ENG': '4p 4d+R1800+ E+S No-red-5',
    },
    '四特南': {
        'DEFAULT': '4 Tokujou Hanchan Nashinashi',
        'EMA_ENG': '4p Upper-dan E+S No-red-5 No-open-all-simples',
        'ENG': '4p 4d+R1800+ E+S No-red-5 No-open-all-simples',
    },
    '三特東喰赤速': {
        'DEFAULT': '3 Tokujou Tonpu Fast',
        'EMA_ENG': '3p Upper-dan E Fast',
        'ENG': '3p 4d+R1800+ E Fast',
    },
    '三特東喰赤': {
        'DEFAULT': '3 Tokujou Tonpu',
        'EMA_ENG': '3p Upper-dan E',
        'ENG': '3p 4d+R1800+ E',
    },
    '三特東喰': {
        'DEFAULT': '3 Tokujou Tonpu Akanashi',
        'EMA_ENG': '3p Upper-dan E No-red-5',
        'ENG': '3p 4d+R1800+ E No-red-5',
    },
    '三特東': {
        'DEFAULT': '3 Tokujou Tonpu Nashinashi',
        'EMA_ENG': '3p Upper-dan E No-red-5 No-open-all-simples',
        'ENG': '3p 4d+R1800+ E No-red-5 No-open-all-simples',
    },
    '三特南喰赤速': {
        'DEFAULT': '3 Tokujou Hanchan Fast',
        'EMA_ENG': '3p Upper-dan E+S Fast',
        'ENG': '3p 4d+R1800+ E+S Fast',
    },
    '三特南喰赤': {
        'DEFAULT': '3 Tokujou Hanchan',
        'EMA_ENG': '3p Upper-dan E+S',
        'ENG': '3p 4d+R1800+ E+S',
    },
    '三特南喰': {
        'DEFAULT': '3 Tokujou Hanchan Akanashi',
        'EMA_ENG': '3p Upper-dan E+S No-red-5',
        'ENG': '3p 4d+R1800+ E+S No-red-5',
    },
    '三特南': {
        'DEFAULT': '3 Tokujou Hanchan Nashinashi',
        'EMA_ENG': '3p Upper-dan E+S No-red-5 No-open-all-simples',
        'ENG': '3p 4d+R1800+ E+S No-red-5 No-open-all-simples',
    },
    '四鳳東喰赤速': {
        'DEFAULT': '4 Houou Tonpu Fast',
        'EMA_ENG': '4p Phoenix E Fast',
        'ENG': '4p 7d+R2000+ E Fast',
    },
    '四鳳東喰赤': {
        'DEFAULT': '4 Houou Tonpu',
        'EMA_ENG': '4p Phoenix E',
        'ENG': '4p 7d+R2000+ E',
    },
    '四鳳東喰': {
        'DEFAULT': '4 Houou Tonpu Akanashi',
        'EMA_ENG': '4p Phoenix E No-red-5',
        'ENG': '4p 7d+R2000+ E No-red-5',
    },
    '四鳳東': {
        'DEFAULT': '4 Houou Tonpu Nashinashi',
        'EMA_ENG': '4p Phoenix E No-red-5 No-open-all-simples',
        'ENG': '4p 7d+R2000+ E No-red-5 No-open-all-simples',
    },
    '四鳳南喰赤速': {
        'DEFAULT': '4 Houou Hanchan Fast',
        'EMA_ENG': '4p 7Phoenix E+S Fast',
        'ENG': '4p 7d+R2000+ E+S Fast',
    },
    '四鳳南喰赤': {
        'DEFAULT': '4 Houou Hanchan',
        'EMA_ENG': '4p Phoenix E+S',
        'ENG': '4p 7d+R2000+ E+S',
    },
    '四鳳南喰': {
        'DEFAULT': '4 Houou Hanchan Akanashi',
        'EMA_ENG': '4p Phoenix E+S No-red-5',
        'ENG': '4p 7d+R2000+ E+S No-red-5',
    },
    '四鳳南': {
        'DEFAULT': '4 Houou Hanchan Nashinashi',
        'EMA_ENG': '4p Phoenix E+S No-red-5 No-open-all-simples',
        'ENG': '4p 7d+R2000+ E+S No-red-5 No-open-all-simples',
    },
    '三鳳東喰赤速': {
        'DEFAULT': '3 Houou Tonpu Fast',
        'EMA_ENG': '3p Phoenix E Fast',
        'ENG': '3p 7d+R2000+ E Fast',
    },
    '三鳳東喰赤': {
        'DEFAULT': '3 Houou Tonpu',
        'EMA_ENG': '3p Phoenix E',
        'ENG': '3p 7d+R2000+ E',
    },
    '三鳳東喰': {
        'DEFAULT': '3 Houou Tonpu Akanashi',
        'EMA_ENG': '3p Phoenix E No-red-5',
        'ENG': '3p 7d+R2000+ E No-red-5',
    },
    '三鳳東': {
        'DEFAULT': '3 Houou Tonpu Nashinashi',
        'EMA_ENG': '3p Phoenix E No-red-5 No-open-all-simples',
        'ENG': '3p 7d+R2000+ E No-red-5 No-open-all-simples',
    },
    '三鳳南喰赤速': {
        'DEFAULT': '3 Houou Hanchan Fast',
        'EMA_ENG': '3p Phoenix E+S Fast',
        'ENG': '3p 7d+R2000+ E+S Fast',
    },
    '三鳳南喰赤': {
        'DEFAULT': '3 Houou Hanchan',
        'EMA_ENG': '3p Phoenix E+S',
        'ENG': '3p 7d+R2000+ E+S',
    },
    '三鳳南喰': {
        'DEFAULT': '3 Houou Hanchan Akanashi',
        'EMA_ENG': '3p Phoenix E+S No-red-5',
        'ENG': '3p 7d+R2000+ E+S No-red-5',
    },
    '三鳳南': {
        'DEFAULT': '3 Houou Hanchan Nashinashi',
        'EMA_ENG': '3p Phoenix E+S No-red-5 No-open-all-simples',
        'ENG': '3p 7d+R2000+ E+S No-red-5 No-open-all-simples',
    },
    '四麻上級卓': {
        'DEFAULT': '4 players, Joukyuu',
        'EMA_ENG': '4p, Lower dan',
        'ENG': '4p, 1k+',
    },
    '三麻上級卓': {
        'DEFAULT': '3 players, Joukyuu',
        'EMA_ENG': '3p, Lower dan',
        'ENG': '3p, 1k+',
    },
    '四麻特上卓': {
        'DEFAULT': '4 players, Tokujou',
        'EMA_ENG': '4p, Upper dan',
        'ENG': '4p, 4d+R1800+',
    },
    '三麻特上卓': {
        'DEFAULT': '3 players, Tokujou',
        'EMA_ENG': '3p, Upper dan',
        'ENG': '3p, 4d+R1800+',
    },
    '四麻雀荘戦': {
        'DEFAULT': '4 players, Jansou',
        'EMA_ENG': '4p, Parlor',
        'ENG': '4p, Parlor',
    },
    '三麻雀荘戦': {
        'DEFAULT': '3 players, Jansou',
        'EMA_ENG': '3p, Parlor',
        'ENG': '3p, Parlor',
    },
    '四麻鳳凰卓': {
        'DEFAULT': '4 players, Houou',
        'EMA_ENG': '4p, Phoenix',
        'ENG': '4p, 7d+R2000+',
    },
    '三麻鳳凰卓': {
        'DEFAULT': '3 players, Houou',
        'EMA_ENG': '3p, Phoenix',
        'ENG': '3p, 7d+R2000+',
    },
    '新人': {
        'DEFAULT': '新人',
        'ENG': 'New',
    },
    '９級': {
        'DEFAULT': '9級',
        'ENG': '9k',
    },
    '８級': {
        'DEFAULT': '8級',
        'ENG': '8k',
    },
    '７級': {
        'DEFAULT': '7級',
        'ENG': '7k',
    },
    '６級': {
        'DEFAULT': '6級',
        'ENG': '6k',
    },
    '５級': {
        'DEFAULT': '5級',
        'ENG': '5k',
    },
    '４級': {
        'DEFAULT': '4級',
        'ENG': '4k',
    },
    '３級': {
        'DEFAULT': '3級',
        'ENG': '3k',
    },
    '２級': {
        'DEFAULT': '2級',
        'ENG': '2k',
    },
    '１級': {
        'DEFAULT': '1級',
        'ENG': '1k',
    },
    '初段': {
        'DEFAULT': '初段',
        'ENG': '1d',
    },
    '二段': {
        'DEFAULT': '二段',
        'ENG': '2d',
    },
    '三段': {
        'DEFAULT': '三段',
        'ENG': '3d',
    },
    '四段': {
        'DEFAULT': '四段',
        'ENG': '4d',
    },
    '五段': {
        'DEFAULT': '五段',
        'ENG': '5d',
    },
    '六段': {
        'DEFAULT': '六段',
        'ENG': '6d',
    },
    '七段': {
        'DEFAULT': '七段',
        'ENG': '7d',
    },
    '八段': {
        'DEFAULT': '八段',
        'ENG': '8d',
    },
    '九段': {
        'DEFAULT': '九段',
        'ENG': '9d',
    },
    '十段': {
        'DEFAULT': '十段',
        'ENG': '10d',
    },
    '天鳳位': {
        'DEFAULT': '天鳳位',
        'ENG': 'Immortal',
    },
    '巡目': {
        'DEFAULT': ' Turn',
    },
    '枚': {
        'DEFAULT': 'shuugi',
    },
    '速': {
        'DEFAULT': ' fast',
    },
    '個室': {
        'DEFAULT': 'Lobby:',
    },
    '大会': {
        'DEFAULT': 'Tournament:',
    },
};

const partialTranslationForStats = {
    '般南喰赤': {
        'DEFAULT': 'Ippan, Hanchan',
        'EMA_ENG': '4p, Novice, E+S',
        'ENG': '4p, Novice, E+S',
    },
    '般東喰赤': {
        'DEFAULT': 'Ippan, Tonpu',
        'EMA_ENG': '4p, Novice, E',
        'ENG': '4p, Novice, E',
    },
    '上南喰赤': {
        'DEFAULT': 'Joukyuu, Hanchan',
        'EMA_ENG': '4p, Lower-dan, E+S',
        'ENG': '4p, 1k+, E+S',
    },
    '上東喰赤': {
        'DEFAULT': 'Joukyuu, Tonpu',
        'EMA_ENG': '4p, Lower-dan, E',
        'ENG': '4p, 1k+, E',
    },
    '特南喰赤': {
        'DEFAULT': 'Tokujou, Hanchan',
        'EMA_ENG': '4p, Upper dan, E+S',
        'ENG': '4p, 4d+R1800+, E+S',
    },
    '特東喰赤': {
        'DEFAULT': 'Tokujou, Tonpu',
        'EMA_ENG': '4p, Upper dan, E',
        'ENG': '4p, 4d+R1800+, E',
    },
    '鳳南喰赤': {
        'DEFAULT': 'Houou, Hanchan',
        'EMA_ENG': '4p, Phoenix, E+S',
        'ENG': '4p, 7d+R2000+, E+S',
    },
    '鳳東喰赤': {
        'DEFAULT': 'Houou, Tonpu',
        'EMA_ENG': '4p, Phoenix, E',
        'ENG': '4p, 7d+R2000+, E',
    },
    '一般': {
        'DEFAULT': 'Ippan',
        'EMA_ENG': 'Novice',
        'ENG': 'Novice',
    },
    '上級': {
        'DEFAULT': 'Tokujou',
        'EMA_ENG': 'Lower dan',
        'ENG': '1k+',
    },
    '特上': {
        'DEFAULT': 'Joukyuu',
        'EMA_ENG': 'Upper dan',
        'ENG': '4d+R1800+',
    },
    '鳳凰': {
        'DEFAULT': 'Houou',
        'EMA_ENG': 'Phoenix',
        'ENG': '7d+R2000+',
    },
    '三東喰赤': {
        'DEFAULT': '3p, Tonpu',
        'EMA_ENG': '3p E',
    },
    '三南喰赤': {
        'DEFAULT': '3p, Hanchan',
        'EMA_ENG': '3p E+S',
    },
    '東喰－': {
        'DEFAULT': 'Tonpu, open tanyao, akanashi',
        'EMA_ENG': 'E, open all-simples, no red 5s',
    },
    '東－－': {
        'DEFAULT': 'Tonpu, no open tanyao, akanashi',
        'EMA_ENG': 'E, no open all-simples, no red 5s',
    },
    '南喰－': {
        'DEFAULT': 'Hanchan, open tanyao, akanashi',
        'EMA_ENG': 'E+S, open all-simples, no red 5s',
    },
    '南－－': {
        'DEFAULT': 'Hanchan, no open tanyao, akanashi',
        'EMA_ENG': 'E+S, no open all-simples, no red 5s',
    },
    '三上東喰赤': {
        'DEFAULT': '3, joukyuu, tonpu, open tanyao, akanashi',
        'EMA_ENG': '3p, E, open all-simples, red 5s',
    },
    '三上南喰赤': {
        'DEFAULT': '3, joukyuu, hanchan, open tanyao, akanashi',
        'EMA_ENG': '3p, E, open all-simples, red 5s',
    },
    '全ルールの役満': {
        'DEFAULT': 'List yakuman only',
        'ENG': 'Limit-hands only',
    },
    '割合%': {
        'DEFAULT': '% of wins',
    },
    '和了数': {
        'DEFAULT': 'No. of winning hands this month:',
    },
    '役統計': {
        'DEFAULT': 'Statistics of yaku',
        'EMA_ENG': 'Statistics of winning hands',
    },
    '東喰赤': {
        'DEFAULT': 'E-only games',
    },
    '南喰赤': {
        'DEFAULT': 'E+S games',
    },
    '複合飜': {
        'DEFAULT': 'Avg.tot.han',
    },
    '%*飜': {
        'DEFAULT': 'win% x han/yaku',
    },
    '%*複': {
        'DEFAULT': 'win% x tot.han',
    },
    '※統計情報は毎月リセットされます': {
        'DEFAULT': 'Statistics are reset monthly',
    },
    '役満': {
        'DEFAULT': 'yakuman',
    },
    '対戦数': {
        'DEFAULT': '# of games',
    },
    '４人打ち全ルール': {
        'DEFAULT': 'list of top-ranked 4p players',
    },
    '３人打ち全ルール': {
        'DEFAULT': 'list of top-ranked 3p players',
    },
    '全ルール': {
        'DEFAULT': ' list of top-ranked players',
    },
    '在位数': {
        'DEFAULT': 'Count',
    },
    '４人打ち': {
        'DEFAULT': '4p',
    },
    '３人打ち': {
        'DEFAULT': '3p',
    },
    '平均R': {
        'DEFAULT': 'Avg.R',
    },
    'Rateランキング': {
        'DEFAULT': 'Players with R1800+',
    },
    '段位': {
        'DEFAULT': 'Rank',
    },
    '名前': {
        'DEFAULT': 'Player',
    },
    '残り': {
        'DEFAULT': 'More: ',
    },
    '件を表示': {
        'DEFAULT': ' - show',
    },
    '満貫未満': {
        'DEFAULT': 'less than mangan',
    },
    '満貫': {
        'DEFAULT': 'mangan',
    },
    '跳満': {
        'DEFAULT': 'haneman',
    },
    '倍満': {
        'DEFAULT': 'baiman',
    },
    '三倍満': {
        'DEFAULT': 'sanbaiman',
    },
    '満貫統計': {
        'DEFAULT': 'mangan-yakuman frequencies',
    },
    '統計': {
        'DEFAULT': 'frequencies',
    },
    '上＝出現率％ (和了数/全和了数*100)': {
        'DEFAULT': 'Upper number: the percentage of winning hands that got this score',
    },
    '下＝１位達成者の割合％ (１位の和了数/１～４位の和了数*100)': {
        'DEFAULT': 'Lower number: Of all the players that won a hand with this score, this is the percentage that went on to get 1st place in that game',
    },
    '統計情報は毎月リセットされます': {
        'DEFAULT': 'Statistics are reset at midnight (JST) on the first day of each calendar month',
    },
};

const tooltips = {
    '位': {
        'DEFAULT': 'Placing',
    },
    '戦': {
        'DEFAULT': 'Games',
    },
    '四般東喰赤速': {
        'DEFAULT': '4 players, Ippan, Tonpuusen, Fast',
        'EMA_ENG': '4 players, Novice, East, Fast',
        'ENG': '4 players, Novice, East, Red-5, Fast',
    },
    '四般東喰赤': {
        'DEFAULT': '4 players, Ippan, Tonpuusen',
        'EMA_ENG': '4 players, Novice, East',
        'ENG': '4 players, Novice, East',
    },
    '四般東喰': {
        'DEFAULT': '4 players, Ippan, Tonpuusen, No akadora',
        'EMA_ENG': '4 players, Novice, East, No red dora',
        'ENG': '4 players, Novice, East, No red dora',
    },
    '四般東': {
        'DEFAULT': '4 players, Ippan, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '4 players, Novice, East, No red dora, No open all-simples',
        'ENG': '4 players, Novice, East, No red dora, No open all-simples',
    },
    '四般南喰赤速': {
        'DEFAULT': '4 players Ippan, Hanchan, Fast',
        'EMA_ENG': '4 players, Novice, East + South, Fast',
        'ENG': '4 players, Novice, East + South, Fast',
    },
    '四般南喰赤': {
        'DEFAULT': '4 players, Ippan, Hanchan',
        'EMA_ENG': '4 players, Novice, East + South',
        'ENG': '4 players, Novice, East + South',
    },
    '四般南喰': {
        'DEFAULT': '4 players, Ippan, Hanchan, No akadora',
        'EMA_ENG': '4 players, Novice, East + South, No red dora',
        'ENG': '4 players, Novice, East + South, No red dora',
    },
    '四般南': {
        'DEFAULT': '4 players, Ippan, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '4 players, Novice, East + South, No red dora, No open all-simples',
        'ENG': '4 players, Novice, East + South, No red dora, No open all-simples',
    },
    '三般東喰赤速': {
        'DEFAULT': '3 players, Ippan, Tonpuusen, Fast',
        'EMA_ENG': '3 players, Novice, East, Fast',
        'ENG': '3 players, Novice, East, Fast',
    },
    '三般東喰赤': {
        'DEFAULT': '3 players, Ippan, Tonpuusen',
        'EMA_ENG': '3 players, Novice, East',
        'ENG': '3 players, Novice, East',
    },
    '三般東喰': {
        'DEFAULT': '3 players, Ippan, Tonpuusen, No akadora',
        'EMA_ENG': '3 players, Novice, East, No red dora',
        'ENG': '3 players, Novice, East, No red dora',
    },
    '三般東': {
        'DEFAULT': '3 players, Ippan, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '3 players, Novice, East, No red dora, No open all-simples',
        'ENG': '3 players, Novice, East, No red dora, No open all-simples',
    },
    '三般南喰赤速': {
        'DEFAULT': '3 players, Ippan, Hanchan, Fast',
        'EMA_ENG': '3 players, Novice, East + South, Fast',
        'ENG': '3 players, Novice, East + South, Fast',
    },
    '三般南喰赤': {
        'DEFAULT': '3 players, Ippan, Hanchan',
        'EMA_ENG': '3 players, Novice, East + South',
        'ENG': '3 players, Novice, East + South',
    },
    '三般南喰': {
        'DEFAULT': '3 players, Ippan, Hanchan, No akadora',
        'EMA_ENG': '3 players, Novice, East, No red dora',
        'ENG': '3 players, Novice, East, No red dora',
    },
    '三般南': {
        'DEFAULT': '3 players, Ippan, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '3 players, Novice, East + South, No red dora, No open all-simples',
        'ENG': '3 players, Novice, East + South, No red dora, No open all-simples',
    },
    '四上東喰赤速': {
        'DEFAULT': '4 players, Joukyuu, Tonpuusen, Fast',
        'EMA_ENG': '4 players, Lower-dan, East, Fast',
        'ENG': '4 players, 1k+, East, Fast',
    },
    '四上東喰赤': {
        'DEFAULT': '4 players, Joukyuu, Tonpuusen',
        'EMA_ENG': '4 players, Lower-dan, East',
        'ENG': '4 players, 1k+, East',
    },
    '四上東喰': {
        'DEFAULT': '4 players, Joukyuu, Tonpuusen, No akadora',
        'EMA_ENG': '4 players, Lower-dan, East, No red dora',
        'ENG': '4 players, 1k+, East, No red dora',
    },
    '四上東': {
        'DEFAULT': '4 players, Joukyuu, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '4 players, Lower-dan, East, No red dora, No open all-simples',
        'ENG': '4 players, 1k+, East, No red dora, No open all-simples',
    },
    '四上南喰赤速': {
        'DEFAULT': '4 players, Joukyuu, Hanchan, Fast',
        'EMA_ENG': '4 players, Lower-dan, East + South, Fast',
        'ENG': '4 players, 1k+, East + South, Fast',
    },
    '四上南喰赤': {
        'DEFAULT': '4 players, Joukyuu, Hanchan',
        'EMA_ENG': '4 players, Lower-dan, East + South',
        'ENG': '4 players, 1k+, East + South',
    },
    '四上南喰': {
        'DEFAULT': '4 players, Joukyuu, Hanchan, No akadora',
        'EMA_ENG': '4 players, Lower-dan, East + South, No red dora',
        'ENG': '4 players, 1k+, East + South, No red dora',
    },
    '四上南': {
        'DEFAULT': '4 players, Joukyuu, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '4 players, Lower-dan, East + South, No red dora, No open all-simples',
        'ENG': '4 players, 1k+, East + South, No red dora, No open all-simples',
    },
    '三上東喰赤速': {
        'DEFAULT': '3 players, Joukyuu, Tonpuusen, Fast',
        'EMA_ENG': '3 players, Lower-dan, East, Fast',
        'ENG': '3 players, 1k+, East, Fast',
    },
    '三上東喰赤': {
        'DEFAULT': '3 players, Joukyuu, Tonpuusen',
        'EMA_ENG': '3 players, Lower-dan, East',
        'ENG': '3 players, 1k+, East',
    },
    '三上東喰': {
        'DEFAULT': '3 players, Joukyuu, Tonpuusen, No akadora',
        'EMA_ENG': '3 players, Lower-dan, East, No red dora',
        'ENG': '3 players, 1k+, East, No red dora',
    },
    '三上東': {
        'DEFAULT': '3 players, Joukyuu, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '3 players, Lower-dan, East, No red dora, No open all-simples',
        'ENG': '3 players, 1k+, East, No red dora, No open all-simples',
    },
    '三上南喰赤速': {
        'DEFAULT': '3 players, Joukyuu, Hanchan, Fast',
        'EMA_ENG': '3 players, Lower-dan, East + South, Fast',
        'ENG': '3 players, 1k+, East + South, Fast',
    },
    '三上南喰赤': {
        'DEFAULT': '3 players, Joukyuu, Hanchan',
        'EMA_ENG': '3 players, Lower-dan, East + South',
        'ENG': '3 players, 1k+, East + South',
    },
    '三上南喰': {
        'DEFAULT': '3 players, Joukyuu, Hanchan, No akadora',
        'EMA_ENG': '3 players, Lower-dan, East + South, No red dora',
        'ENG': '3 players, 1k+, East + South, No red dora',
    },
    '三上南': {
        'DEFAULT': '3 players, Joukyuu, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '3 players, Lower-dan, East + South, No red dora, No open all-simples',
        'ENG': '3 players, 1k+, East + South, No red dora, No open all-simples',
    },
    '四特東喰赤速': {
        'DEFAULT': '4 players, Tokujou, Tonpuusen, Fast',
        'EMA_ENG': '4 players, Upper-dan, East, Fast',
        'ENG': '4 players, 4d+R1800+, East, Fast',
    },
    '四特東喰赤': {
        'DEFAULT': '4 players, Tokujou, Tonpuusen',
        'EMA_ENG': '4 players, Upper-dan, East',
        'ENG': '4 players, 4d+R1800+, East',
    },
    '四特東喰': {
        'DEFAULT': '4 players, Tokujou, Tonpuusen, No akadora',
        'EMA_ENG': '4 players, Upper-dan, East, No red dora',
        'ENG': '4 players, 4d+R1800+, East, No red dora',
    },
    '四特東': {
        'DEFAULT': '4 players, Tokujou, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '4 players, Upper-dan, East, No red dora, No open all-simples',
        'ENG': '4 players, 4d+R1800+, East, No red dora, No open all-simples',
    },
    '四特南喰赤速': {
        'DEFAULT': '4 players, Tokujou, Hanchan, Fast',
        'EMA_ENG': '4 players, Upper-dan, East + South, Fast',
        'ENG': '4 players, 4d+R1800+, East + South, Fast',
    },
    '四特南喰赤': {
        'DEFAULT': '4 players, Tokujou, Hanchan',
        'EMA_ENG': '4 players, Upper-dan, East + South',
        'ENG': '4 players, 4d+R1800+, East + South',
    },
    '四特南喰': {
        'DEFAULT': '4 players, Tokujou, Hanchan, No akadora',
        'EMA_ENG': '4 players, Upper-dan, East + South, No red dora',
        'ENG': '4 players, 4d+R1800+, East + South, No red dora',
    },
    '四特南': {
        'DEFAULT': '4 players, Tokujou, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '4 players, Upper-dan, East + South, No red dora, No open all-simples',
        'ENG': '4 players, 4d+R1800+, East + South, No red dora, No open all-simples',
    },
    '三特東喰赤速': {
        'DEFAULT': '3 players, Tokujou, Tonpuusen, Fast',
        'EMA_ENG': '3 players, Upper-dan, East, Fast',
        'ENG': '3 players, 4d+R1800+, East, Fast',
    },
    '三特東喰赤': {
        'DEFAULT': '3 players, Tokujou, Tonpuusen',
        'EMA_ENG': '3 players, Upper-dan, East',
        'ENG': '3 players, 4d+R1800+, East',
    },
    '三特東喰': {
        'DEFAULT': '3 players, Tokujou, Tonpuusen, No akadora',
        'EMA_ENG': '3 players, Upper-dan, East, No red dora',
        'ENG': '3 players, 4d+R1800+, East, No red dora',
    },
    '三特東': {
        'DEFAULT': '3 players, Tokujou, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '3 players, Upper-dan, East, No red dora, No open all-simples',
        'ENG': '3 players, 4d+R1800+, East, No red dora, No open all-simples',
    },
    '三特南喰赤速': {
        'DEFAULT': '3 players, Tokujou, Hanchan, Fast',
        'EMA_ENG': '3 players, Upper-dan, East + South, Fast',
        'ENG': '3 players, 4d+R1800+, East + South, Fast',
    },
    '三特南喰赤': {
        'DEFAULT': '3 players, Tokujou, Hanchan',
        'EMA_ENG': '3 players, Upper-dan, East + South',
        'ENG': '3 players, 4d+R1800+, East + South',
    },
    '三特南喰': {
        'DEFAULT': '3 players, Tokujou, Hanchan, No akadora',
        'EMA_ENG': '3 players, Upper-dan, East + South, No red dora',
        'ENG': '3 players, 4d+R1800+, East + South, No red dora',
    },
    '三特南': {
        'DEFAULT': '3 players, Tokujou, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '3 players, Upper-dan, East + South, No red dora, No open all-simples',
        'ENG': '3 players, 4d+R1800+, East + South, No red dora, No open all-simples',
    },
    '四鳳東喰赤速': {
        'DEFAULT': '4 players, Houou, Tonpuusen, Fast',
        'EMA_ENG': '4 players, Phoenix, East, Fast',
        'ENG': '4 players, 7d+R2000+, East, Fast',
    },
    '四鳳東喰赤': {
        'DEFAULT': '4 players, Houou, Tonpuusen',
        'EMA_ENG': '4 players, Phoenix, East',
        'ENG': '4 players, 7d+R2000+, East',
    },
    '四鳳東喰': {
        'DEFAULT': '4 players, Houou, Tonpuusen, No akadora',
        'EMA_ENG': '4 players, Phoenix, East, No red dora',
        'ENG': '4 players, 7d+R2000+, East, No red dora',
    },
    '四鳳東': {
        'DEFAULT': '4 players, Houou, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '4 players, Phoenix, East, No red dora, No open all-simples',
        'ENG': '4 players, 7d+R2000+, East, No red dora, No open all-simples',
    },
    '四鳳南喰赤速': {
        'DEFAULT': '4 players, Houou, Hanchan, Fast',
        'EMA_ENG': '4 players, 7Phoenix, East + South, Fast',
        'ENG': '4 players, 7d+R2000+, East + South, Fast',
    },
    '四鳳南喰赤': {
        'DEFAULT': '4 players, Houou, Hanchan',
        'EMA_ENG': '4 players, Phoenix, East + South',
        'ENG': '4 players, 7d+R2000+, East + South',
    },
    '四鳳南喰': {
        'DEFAULT': '4 players, Houou, Hanchan, No akadora',
        'EMA_ENG': '4 players, Phoenix, East + South, No red dora',
        'ENG': '4 players, 7d+R2000+, East + South, No red dora',
    },
    '四鳳南': {
        'DEFAULT': '4 players, Houou, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '4 players, Phoenix, East + South, No red dora, No open all-simples',
        'ENG': '4 players, 7d+R2000+, East + South, No red dora, No open all-simples',
    },
    '三鳳東喰赤速': {
        'DEFAULT': '3 players, Houou, Tonpuusen, Fast',
        'EMA_ENG': '3 players, Phoenix, East, Fast',
        'ENG': '3 players, 7d+R2000+, East, Fast',
    },
    '三鳳東喰赤': {
        'DEFAULT': '3 players, Houou, Tonpuusen',
        'EMA_ENG': '3 players, Phoenix, East',
        'ENG': '3 players, 7d+R2000+, East',
    },
    '三鳳東喰': {
        'DEFAULT': '3 players, Houou, Tonpuusen, No akadora',
        'EMA_ENG': '3 players, Phoenix, East, No red dora',
        'ENG': '3 players, 7d+R2000+, East, No red dora',
    },
    '三鳳東': {
        'DEFAULT': '3 players, Houou, Tonpuusen, No akadora, No kuitan',
        'EMA_ENG': '3 players, Phoenix, East, No red dora, No open all-simples',
        'ENG': '3 players, 7d+R2000+, East, No red dora, No open all-simples',
    },
    '三鳳南喰赤速': {
        'DEFAULT': '3 players, Houou, Hanchan, Fast',
        'EMA_ENG': '3 players, Phoenix, East + South, Fast',
        'ENG': '3 players, 7d+R2000+, East + South, Fast',
    },
    '三鳳南喰赤': {
        'DEFAULT': '3 players, Houou, Hanchan',
        'EMA_ENG': '3 players, Phoenix, East + South',
        'ENG': '3 players, 7d+R2000+, East + South',
    },
    '三鳳南喰': {
        'DEFAULT': '3 players, Houou, Hanchan, No akadora',
        'EMA_ENG': '3 players, Phoenix, East + South, No red dora',
        'ENG': '3 players, 7d+R2000+, East + South, No red dora',
    },
    '三鳳南': {
        'DEFAULT': '3 players, Houou, Hanchan, No akadora, No kuitan',
        'EMA_ENG': '3 players, Phoenix, East + South, No red dora, No open all-simples',
        'ENG': '3 players, 7d+R2000+, East + South, No red dora, No open all-simples',
    },
    '新人': {
        'DEFAULT': 'rookie',
        'ENG': null,
    },
    '９級': {
        'DEFAULT': '9 kyuu',
        'ENG': null,
    },
    '８級': {
        'DEFAULT': '8 kyuu',
        'ENG': null,
    },
    '７級': {
        'DEFAULT': '7 kyuu',
        'ENG': null,
    },
    '６級': {
        'DEFAULT': '6 kyuu',
        'ENG': null,
    },
    '５級': {
        'DEFAULT': '5 kyuu',
        'ENG': null,
    },
    '４級': {
        'DEFAULT': '4 kyuu',
        'ENG': null,
    },
    '３級': {
        'DEFAULT': '3 kyuu',
        'ENG': null,
    },
    '２級': {
        'DEFAULT': '2 kyuu',
        'ENG': null,
    },
    '１級': {
        'DEFAULT': '1 kyuu',
        'ENG': null,
    },
    '初段': {
        'DEFAULT': '1 dan',
        'ENG': null,
    },
    '二段': {
        'DEFAULT': '2 dan',
        'ENG': null,
    },
    '三段': {
        'DEFAULT': '3 dan',
        'ENG': null,
    },
    '四段': {
        'DEFAULT': '4 dan',
        'ENG': null,
    },
    '五段': {
        'DEFAULT': '5 dan',
        'ENG': null,
    },
    '六段': {
        'DEFAULT': '6 dan',
        'ENG': null,
    },
    '七段': {
        'DEFAULT': '7 dan',
        'ENG': null,
    },
    '八段': {
        'DEFAULT': '8 dan',
        'ENG': null,
    },
    '九段': {
        'DEFAULT': '9 dan',
        'ENG': null,
    },
    '十段': {
        'DEFAULT': '10 dan',
        'ENG': null,
    },
    '天鳳位': {
        'DEFAULT': 'Tenhoui',
        'ENG': null,
    },
};
